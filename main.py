import json

import flask
from flask import jsonify

app = flask.app.Flask(__name__)

@app.get('/')
def index():
    return '<h1>Hello World!</h1>'

@app.route('/api/v1/users/')
def users():
    return jsonify([1,2,3,4,5])

app.run(port=7090)